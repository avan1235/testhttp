#ifndef TESTHTTP_RAW_UTIL_H
#define TESTHTTP_RAW_UTIL_H

#include <stdbool.h>
#include <zconf.h>
#include <stdlib.h>
#include <stdio.h>

#include "constants.h"

#define DEBUG_PRINT(data...) do { if (DEBUG) { fprintf(stderr, data); } } while (false)

/**
 * Compare if pre matches as a prefix of str
 * @param pre string to be matched
 * @param str to be searched in
 * @return true on match otherwise false
 */
bool prefix_cmp(const char *pre, const char *str);

/**
 * Same as prefix_cmp but case insensitive
 * @param pre string to be matched
 * @param str to be searched in
 * @return true on match ignorig letters case otherwise false
 */
bool prefix_cmp_case(const char *pre, const char *str);

/**
 * Check if specified position in buffer is CRLF
 * @param buffer position to be checked
 * @return true on match otherwise false
 */
bool is_line_end(const char *buffer);

/**
 * Print single char
 * @param c to be printed
 */
void print_char(char c);

/**
 * Print prefix of specified length from buffer.
 * The value must be valid buffer index
 * @param buffer to be printed partially
 * @param count number of chars to print
 */
void print_prefix(const char *buffer, size_t count);

#endif //TESTHTTP_RAW_UTIL_H
