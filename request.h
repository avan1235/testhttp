#ifndef TESTHTTP_RAW_REQUEST_H
#define TESTHTTP_RAW_REQUEST_H

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>

#include "util.h"
#include "err.h"
#include "constants.h"

/**
 * Create HTTP/1.1 GET request body with specified host and location on server
 * with the cookies data from opened file which contains only correct cookies data
 * Request form is
 * GET location HTTP/1.1\r\n
 * Host: hostname\r\n
 * (Cookie: KEY=VALUE\r\n)*
 * Connection: close\r\n\r\n
 * @param file to read cookies from (cookies must be in valid form 'name=value' per line) but file can be empty
 * @param address to be in request as the hostname and location
 */
void send_request(FILE *file, char *address, int sock);

/**
 * Read new data from socket and validate if read was successfully finished
 * @param buffer pointer to begin of buffer memory
 * @param nbytes number of bytes to be read from socket to buffer
 * @param sock to read from
 * @return number of bytes read from socket to buffer
 */
ssize_t read_data(char *buffer, size_t nbytes, int sock);

#endif //TESTHTTP_RAW_REQUEST_H
