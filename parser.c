#include "parser.h"

static const char *KEY_TRANSFER_ENCODING = KEY_TRANSFER_ENCODING_DEFINE;
static const size_t KEY_TRANSFER_ENCODING_LEN = strlen(KEY_TRANSFER_ENCODING_DEFINE);
static const char *KEY_SET_COOKIE = KEY_SET_COOKIE_DEFINE;
static const size_t KEY_SET_COOKIE_LEN = strlen(KEY_SET_COOKIE_DEFINE);

static const char *CHUNKED = CHUNKED_DEFINE;
static const char *HTTP_NAME = HTTP_NAME_DEFINE;

static const char *OK_CODE = OK_CODE_DEFINE;

/**
 * Check if char is a digit
 * @param c to be checked
 * @return true if digit otherwise false
 */
static bool is_digit(char c)
{
    return c >= '0' && c <= '9';
}

/**
 * Check if char is a digit hex
 * @param c to be checked
 * @return true if hex digit otherwise false
 */
static bool is_hex(char c)
{
    return is_digit(c) || (c >= 'a' && c <= 'f') || (c >= 'A' && c <= 'F');
}

/**
 * Check if char is a whitespace
 * @param c to be checked
 * @return true if whitespace otherwise false
 */
static bool is_ows(char c)
{
    return c == ' ' || c == '\t';
}

/**
 * Check if char is a transfer-encoding delimiter
 * @param c to be checked
 * @return true if valid delimiter otherwise false
 */
static bool is_transfer_encoding_delimiter(char c)
{
    return c == ',';
}

/**
 * Check if char is a cookies delimiter
 * @param c to be checked
 * @return true if cookies delimiter otherwise false
 */
static bool is_cookies_delimiter(char c)
{
    return c == ';';
}

/**
 * Given the number in hex format WITHOUT the prefix function tries to convert
 * this value to long long numerical value (and exists with error on too big numbers
 * to fit in long long variable)
 * @param hex_number to be parsed as string number data
 * @return parsed value of number in decimal
 */
static long long hex_to_number(char *hex_number)
{
    char *endptr;
    errno = 0;
    long long value = strtoll(hex_number, &endptr, 16);
    if ((errno == ERANGE && (value == LONG_MAX || value == LONG_MIN))
        || (errno != 0 && value == 0)) {
        syserr("strtoll");
    }

    if (endptr == hex_number) {
        fatal("no digits were found");
    }
    return value;
}

/**
 * Increment buffer position to eol and go to the next line if possible with position
 * @param buffer begin of buffer data
 * @param buffer_position to be incremented
 * @param read_len max value of increment operation
 */
static void increment_header_to_eol(char *buffer, ssize_t *buffer_position, ssize_t read_len)
{
    while (!is_line_end(buffer + *buffer_position) && *buffer_position < read_len) {
        ++(*buffer_position);
    }
    if (*buffer_position == read_len && !is_line_end(buffer + *buffer_position)) {
        fatal("header buffer of size %d was too small (during increment)", RESPONSE_BUFFER_SIZE);
    }
    *buffer_position += CRLF_LEN;
}

/**
 * Inrement buffer posiiton and read new data from socket if needed
 * @param buffer begin of buffer memory
 * @param buffer_position to be incremented
 * @param read_len last read size of buffer to be changed
 * @param socket to read data from
 * @param expected_length counter of expected bytes to line ending
 * @return true if after finished reading on line end otherwise false
 */
static bool increment_to_eol_and_read(char *buffer, ssize_t *buffer_position, ssize_t *read_len, int socket, size_t expected_length)
{
    while (expected_length > 0 && *buffer_position < *read_len) {
        ++(*buffer_position);
        --expected_length;
    }
    if (*buffer_position == *read_len) {
        *read_len = read_data(buffer, RESPONSE_BUFFER_SIZE, socket);
        *buffer_position = 0;
        return increment_to_eol_and_read(buffer, buffer_position, read_len, socket, expected_length);
    }
    bool line_end = is_line_end(buffer + *buffer_position);
    *buffer_position += CRLF_LEN;
    return expected_length == 0 && line_end;
}

/**
 * Parse encoding data from header buffer by searching for the 'chunked' string
 * and ignoring other values in the specified line
 * @param buffer begin of buffer memory
 * @param buffer_position position in buffer to be incremented
 * @param read_len max position in buffer
 * @return true if found chunked encoding otherwise false
 */
static bool parse_encoding(char *buffer, ssize_t *buffer_position, ssize_t read_len)
{
    bool chunked = false;
    while (true) {
        if (prefix_cmp(CHUNKED, buffer + *buffer_position)) {
            chunked = true;
            increment_header_to_eol(buffer, buffer_position, read_len);
            break;
        }

        while (!is_transfer_encoding_delimiter(buffer[*buffer_position])
               && !is_line_end(buffer + *buffer_position) && *buffer_position < read_len) {
            ++(*buffer_position);
        }
        if (*buffer_position == read_len) {
            fatal("header buffer of size %d was too small (during Transfer-Encoding parse)", RESPONSE_BUFFER_SIZE);
        }
        else if (is_line_end(buffer + *buffer_position)) {
            *buffer_position += CRLF_LEN;
            break;
        }
        else if (is_transfer_encoding_delimiter(buffer[*buffer_position])) {
            ++(*buffer_position);
            *buffer_position += is_ows(buffer[*buffer_position]) ? 1 : 0;
            continue;
        }
    }
    return chunked;
}

/**
 * Read and print cookie data without validating it and try to find the
 * cookie separator to stop printing the cookie
 * @param buffer to read data from
 * @param buffer_positionfrom which printing starts
 * @param read_len max value of position in buffer
 */
static void read_cookie_and_print(char *buffer, ssize_t *buffer_position, ssize_t read_len)
{
    while (!is_cookies_delimiter(buffer[*buffer_position]) && !is_line_end(buffer + *buffer_position) && *buffer_position < read_len) {
        print_char(buffer[*buffer_position]);
        ++(*buffer_position);
    }
    print_char(EOL);
    if (*buffer_position == read_len) {
        fatal("header buffer of size %d was too small (during cookie parse)", RESPONSE_BUFFER_SIZE);
    }
    else {
        increment_header_to_eol(buffer, buffer_position, read_len);
    }

}

char *separate_port(char *address_port) {
    size_t last_index = 0;
    bool found = false;
    for (size_t i = 0; i < strlen(address_port); ++i) {
        if (address_port[i] == ADDRESS_PORT_SEP) {
            last_index = i;
            found = true;
        }
    }

    if (!found) {
        fatal("port or address not specified");
    }

    address_port[last_index] = EOS; // mark first part as the end of the address string data
    return address_port + last_index + 1;
}

bool parse_status_line(char *buffer, ssize_t read_len, ssize_t *buffer_position)
{
    if (read_len < STATUS_LINE_MIN_LEN) {
        fatal("invalid status line length");
    }
    const size_t name_len = strlen(HTTP_NAME);
    if (!prefix_cmp(HTTP_NAME, buffer) || buffer[name_len] != PATH_BR
        || !is_digit(buffer[name_len + 1]) || !is_digit(buffer[name_len + 3])
        || buffer[name_len + 2] != DOT) {
        fatal("invalid http version name");
    }
    if (buffer[name_len + 4] != SP || buffer[name_len + 8] != SP) {
        fatal("invalid spacing in status line");
    }
    ssize_t checked_index = name_len + 5;
    for (size_t i = 0; i < 3; ++i) {
        if (!is_digit(buffer[checked_index])) {
            fatal("invalid http code format");
        }
        ++checked_index;
    }
    bool find_crlf = false;
    while (checked_index < read_len && !find_crlf) {
        if (is_line_end(buffer + checked_index)) {
            find_crlf = true;
        }
        ++checked_index;
    }
    if (!find_crlf) {
        fatal("status line not finished in buffor data (too long)");
    }
    *buffer_position = checked_index + 1; // as the CRLF is 2 characters long
    return prefix_cmp(OK_CODE, buffer + name_len + 5);
}

bool parse_header(char *buffer, ssize_t *buffer_position, ssize_t read_len)
{
    char *line_begin_ptr = buffer + *buffer_position;
    bool chunked = false;
    while (!is_line_end(line_begin_ptr)) { // line starts with CRLF so end of header
        if (prefix_cmp_case(KEY_SET_COOKIE, line_begin_ptr)) {
            *buffer_position += KEY_SET_COOKIE_LEN + (is_ows(line_begin_ptr[KEY_SET_COOKIE_LEN]) ? 1 : 0);
            read_cookie_and_print(buffer, buffer_position, read_len);
        }
        else if (prefix_cmp_case(KEY_TRANSFER_ENCODING, line_begin_ptr)) {
            *buffer_position += KEY_TRANSFER_ENCODING_LEN + (is_ows(line_begin_ptr[KEY_TRANSFER_ENCODING_LEN]) ? 1 : 0);
            chunked = chunked || parse_encoding(buffer, buffer_position, read_len);
        }
        else {
            increment_header_to_eol(buffer, buffer_position, read_len);
        }
        line_begin_ptr = buffer + *buffer_position;
    }
    *buffer_position += CRLF_LEN;
    return chunked;
}

size_t parse_chunked_message_body(char *buffer, ssize_t *buffer_position, ssize_t read_len, int sock)
{
    size_t length = 0;
    bool is_length_part = true;
    bool finished = false;
    size_t curr_length = 0;
    while (!finished) {
        char *current_line_begin = buffer + *buffer_position;
        if (is_length_part) {
            while (is_hex(buffer[*buffer_position])) {
                ++(*buffer_position);
            }
            if (is_line_end(buffer + *buffer_position)) {
                buffer[*buffer_position] = EOS;
                curr_length = hex_to_number(current_line_begin);
                *buffer_position += CRLF_LEN;
                length += curr_length;
                if (curr_length == 0) {
                    finished = true;
                }
            }
            else if (buffer[*buffer_position] == EOS) {
                size_t parsed_len = strlen(current_line_begin);
                strcpy(buffer, current_line_begin);
                read_len = parsed_len + read_data(buffer + parsed_len, RESPONSE_BUFFER_SIZE - parsed_len, sock);
                current_line_begin = buffer;
                *buffer_position = parsed_len;
                while (is_hex(buffer[*buffer_position])) {
                    ++(*buffer_position);
                }
                if (is_line_end(buffer + *buffer_position)) {
                    buffer[*buffer_position] = EOS;
                    curr_length = hex_to_number(current_line_begin);
                    length += curr_length;
                    *buffer_position += CRLF_LEN;
                    if (curr_length == 0) {
                        finished = true;
                    }
                }
                else {
                    fatal("buffer to short to fit received hex number");
                }
            }
            else {
                fatal("invalid hex number format in chunked data");
            }

            is_length_part = !is_length_part;
        }
        else {
            if(!increment_to_eol_and_read(buffer, buffer_position, &read_len, sock, curr_length)) {
                fatal("specified by host chunked length doesn't match real length");
            }
            is_length_part = !is_length_part;
        }
    }
    return length;
}

size_t parse_not_chunked_message_body(char *buffer, const ssize_t *buffer_position, ssize_t read_len, int sock)
{
    size_t length = read_len - *buffer_position;
    while ((read_len = read_data(buffer, RESPONSE_BUFFER_SIZE, sock)) > 0) {
        length += read_len;
    }
    return length;
}