#include "request.h"

static const char *HTTP_URI_START = "http://";
static const char *HTTPS_URI_START = "https://";

/**
 * Extract host and server location query from specified tested address.
 * Query should be used first because this function not allocates memory
 * and uses the address memory so the EOS has to be set after using query
 * data to indicate the end of host data
 * @param address to be parsed
 * @param host to be set to result host
 * @param query to be set to result server location
 */
static void get_host_and_query(char *address, char **host, char **query)
{
    bool http = prefix_cmp(HTTP_URI_START, address);
    bool https = prefix_cmp(HTTPS_URI_START, address);
    size_t index = 0;
    if (http) {
        index = strlen(HTTP_URI_START);
    }
    else if (https) {
        index = strlen(HTTPS_URI_START);
    }
    else {
        fatal("not a valid http address");
    }
    *host = address + index;
    for (; index < strlen(address); ++index) {
        if (address[index] == PATH_BR) {
            *query = address + index;
            return;
        }
    }
    *query = NULL;
}

/**
 * Write data to socket and check writing result - raise system error
 * on write problems
 * @param sock to write data to
 * @param data to be sent to socket
 */
static void write_checked(int sock, const char *data)
{
    ssize_t write_len = write(sock, data, strlen(data));

    if (write_len == -1) {
        syserr("write");
    }
}

void send_request(FILE *file, char *address, int sock)
{
    char *query, *host;
    get_host_and_query(address, &host, &query);

    DEBUG_PRINT("Request header\n");
    write_checked(sock, GET);
    DEBUG_PRINT(GET);

    if (query != NULL) {
        write_checked(sock, query);
        DEBUG_PRINT("%s", query);
        *query = EOS;
    }
    else {
        write_checked(sock, MAIN_PATH);
        DEBUG_PRINT("%s", MAIN_PATH);
    }

    write_checked(sock, HTTP_HOST);
    DEBUG_PRINT(HTTP_HOST);
    write_checked(sock, host);
    DEBUG_PRINT("%s", host);

    char *line = NULL;
    size_t len = 0;
    ssize_t read;
    errno = 0;
    if ((read = getline(&line, &len, file)) > 0) {
        if (line[read - 1] == EOL) {
            line[read - 1] = EOS;
        }
        write_checked(sock, COOKIE_PREFIX);
        DEBUG_PRINT(COOKIE_PREFIX);
        write_checked(sock, line);
        DEBUG_PRINT("%s", line);

        errno = 0;
        while ((read = getline(&line, &len, file)) > 0) {
            if (line[read - 1] == EOL) {
                line[read - 1] = EOS;
            }
            write_checked(sock, COOKIE_DELIM);
            DEBUG_PRINT(COOKIE_DELIM);
            write_checked(sock, line);
            DEBUG_PRINT("%s", line);
        }
    }
    free(line);
    if (errno == ENOMEM) {
        syserr("getline memory error");
    }

    write_checked(sock, CONN_CLOSE);
    DEBUG_PRINT(CONN_CLOSE);
}

ssize_t read_data(char *buffer, size_t nbytes, int sock)
{
    ssize_t read_len = read(sock, buffer, nbytes);
    if (read_len == -1) {
        close(sock);
        syserr("read from socket");
    }
    buffer[read_len] = EOS;
    return read_len;
}