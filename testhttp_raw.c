#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "err.h"
#include "util.h"
#include "request.h"
#include "parser.h"

int main(int argc, char *argv[])
{
    int sock, err;
    ssize_t read_len;
    char *extracted_port, *hostname;
    FILE *cookies_file;
    struct addrinfo addr_hints, *addr_result;

    if (argc != 4) {
        fatal("Usage: %s <connection address>:<connection port> <cookies file> <http address to be tested>\n", argv[0]);
    }
    DEBUG_PRINT("Running: %s %s %s %s\n", argv[0], argv[1], argv[2], argv[3]);

    sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP); // IPv4 TCP/IP
    if (sock < 0) {
        syserr("socket");
    }

    memset(&addr_hints, 0, sizeof(struct addrinfo));
    addr_hints.ai_flags = 0;
    addr_hints.ai_family = AF_INET;
    addr_hints.ai_socktype = SOCK_STREAM;
    addr_hints.ai_protocol = IPPROTO_TCP;

    extracted_port = separate_port(argv[1]);

    hostname = argv[1];
    err = getaddrinfo(hostname, extracted_port, &addr_hints, &addr_result);

    if (err == EAI_SYSTEM) { // system error
        syserr("getaddrinfo: %s", gai_strerror(err));
    }
    else if (err != 0) { // other error (host not found, etc.)
        fatal("getaddrinfo: %s", gai_strerror(err));
    }

    if (connect(sock, addr_result->ai_addr, addr_result->ai_addrlen) < 0) {
        syserr("connect");
    }

    cookies_file = fopen(argv[2], "r");
    if (cookies_file == NULL) {
        fatal("cookies file open error");
    }

    send_request(cookies_file, argv[3], sock);

    if (fclose(cookies_file) == EOF) {
        fatal("cookies file close error");
    }

    char buffer[RESPONSE_BUFFER_SIZE + 1];
    ssize_t buffer_position;
    bool ok_code = false;

    // reading the header part
    read_len = read_data(buffer, RESPONSE_BUFFER_SIZE, sock);
    ok_code = parse_status_line(buffer, read_len, &buffer_position);

    if (!ok_code) {
        print_prefix(buffer + HTTP_PREFIX_LEN, buffer_position - CRLF_LEN - HTTP_PREFIX_LEN);
        print_char(EOL);
    }
    else {
        bool chunked = parse_header(buffer, &buffer_position, read_len);
        size_t length = chunked ? parse_chunked_message_body(buffer, &buffer_position, read_len, sock)
                                : parse_not_chunked_message_body(buffer, &buffer_position, read_len, sock);
        printf("%s%zu\n", RAPORT_TITLE, length);
    }

    if (close(sock) < 0) {
        syserr("closing stream socket");
    }

    return 0;
}