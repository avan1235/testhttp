CC = gcc
CFLAGS = -Wall -Wextra
TARGETS = testhttp_raw

all: $(TARGETS)

err.o: err.c err.h
util.o: util.c util.h constants.h
request.o: request.c request.h util.h constants.h err.h
parser.o: parser.c parser.h util.h constants.h err.h request.h
testhttp_raw.o: testhttp_raw.c parser.h request.h util.h constants.h err.h

testhttp_raw: testhttp_raw.o err.o util.o request.o parser.o constants.h

.PHONY: clean

clean:
	rm -f *.o $(TARGETS)


