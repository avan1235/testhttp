#ifndef TESTHTTP_RAW_CONSTANTS_H
#define TESTHTTP_RAW_CONSTANTS_H

#include <stdbool.h>

#define CR '\r' /// carriage return character

#define EOL '\n' /// end of line character
#define EOS '\0' /// end of string character
#define PATH_BR '/' /// path breaking character
#define DOT '.' /// dot separation character
#define SP ' ' /// space character
#define ADDRESS_PORT_SEP ':' /// character for addres <-> port separation

#define CRLF_LEN 2
#define HTTP_PREFIX_LEN 9

#define STATUS_LINE_MIN_LEN 15

#define RESPONSE_BUFFER_SIZE 4194304  /// must be greater than STATUS_LINE_MIN_LEN, defines max header size in project = 4MB

/// Key that are searched in header
#define KEY_TRANSFER_ENCODING_DEFINE "Transfer-Encoding:"
#define KEY_SET_COOKIE_DEFINE "Set-Cookie:"
#define CHUNKED_DEFINE "chunked"
#define OK_CODE_DEFINE "200"
#define HTTP_NAME_DEFINE "HTTP"

#define RAPORT_TITLE "Dlugosc zasobu: "

/// Request definition with placeholders
#define GET "GET "
#define HTTP_HOST " HTTP/1.1\r\nHost: "
#define COOKIE_PREFIX "\r\nCookie: "
#define COOKIE_DELIM "; "
#define CONN_CLOSE "\r\nConnection: close\r\n\r\n"
#define MAIN_PATH "/"

#define DEBUG false

#endif //TESTHTTP_RAW_CONSTANTS_H
