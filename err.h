#ifndef TESTHTTP_RAW_ERR_H
#define TESTHTTP_RAW_ERR_H

/**
 * Get information about failure system function call
 * and exit program with EXIT_FAILURE code
 * @param fmt format to be printed as errors
 * @param ... printed data
 */
extern void syserr(const char *fmt, ...);

/**
 * Print message about the error and exit program with
 * EXIT_FAILURE code
 * @param fmt message to be printed
 * @param ... printed data
 */
extern void fatal(const char *fmt, ...);

#endif //TESTHTTP_RAW_ERR_H
