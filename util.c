#include "util.h"

/**
 * Check if specified char is a letter
 * @param c to be checked
 * @return true for letter otherwise false
 */
static bool is_alpha(char c)
{
    return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
}

/**
 * Unify letters to lower case
 * @param c to be unified
 * @return lower case version of char
 */
static char to_lower_case(char c)
{
    if (c >= 'A' && c <= 'Z') {
        return c + ('a' - 'A');
    }
    else {
        return c;
    }
}

bool prefix_cmp(const char *pre, const char *str)
{
    while(*pre) {
        if(*pre++ != *str++) {
            return false;
        }
    }
    return true;
}

bool prefix_cmp_case(const char *pre, const char *str)
{
    while(*pre) {
        if(*pre++ != *str++
            && (is_alpha(*pre) && is_alpha(*str) && to_lower_case(*pre) != to_lower_case(*str))) {
            return false;
        }
    }
    return true;
}

bool is_line_end(const char *buffer)
{
    if (buffer[0] != CR) {
        return false;
    }
    if (buffer[1] != EOL) {
        return false;
    }
    return true;
}

void print_char(char c)
{
    printf("%c", c);
}

void print_prefix(const char *buffer, size_t count)
{
    for (size_t i = 0; i < count; ++i) {
        print_char(buffer[i]);
    }
}