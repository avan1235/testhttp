#ifndef TESTHTTP_RAW_PARSER_H
#define TESTHTTP_RAW_PARSER_H

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#include "constants.h"
#include "request.h"
#include "util.h"
#include "err.h"

/**
 * Separated address port string from given '<address>:<port>' string valid value
 * and mark the '<address>' part as the separate string by changing the ':' sign to EOS
 * @param address_port string to extract data from
 * @return pointer to begging of the valid port string
 */
char *separate_port(char *address_port);

/**
 * Parse status line from header in buffer and check if it has valid format
 * with http version and status code
 * @param buffer pointer to begin of buffer memory
 * @param read_len size of data read to buffer
 * @param buffer_position where the status line begins and which is incremented
 * @return true on code 200, otherwise false
 */
bool parse_status_line(char *buffer, ssize_t read_len, ssize_t *buffer_position);

/**
 * Parse header of defined max size which should be fully loaded to buffer
 * and try to get the data from it for Transfer-Encoding and Set-Cookie fields
 * @param buffer pointer to begin of buffer memory
 * @param buffer_position to be incremented
 * @param read_len of data in buffer
 * @return true when chunked encoding detected otherwise false
 */
bool parse_header(char *buffer, ssize_t *buffer_position, ssize_t read_len);

/**
 * Read next data from server as chunked and check their length if matches
 * the specified sizes by serwer utinl 'zero chunk' reached
 * @param buffer pointer to begin of buffer memory
 * @param buffer_position to be incremented in buffer
 * @param read_len last read to buffer data length
 * @param sock to read data from
 * @return length of received data in bytes
 */
size_t parse_chunked_message_body(char *buffer, ssize_t *buffer_position, ssize_t read_len, int sock);

/**
 * Read data from server and count the received bytes
 * @param buffer pointer to begin of buffer memory
 * @param buffer_position to be incremented in buffer
 * @param read_len last read to buffer data length
 * @param sock to read data from
 * @return length of received data in bytes
 */
size_t parse_not_chunked_message_body(char *buffer, const ssize_t *buffer_position, ssize_t read_len, int sock);

#endif //TESTHTTP_RAW_PARSER_H
