# Test HTTP project

## Project description

The main feature of this project is to create the software that gives the user ability to test the WWW pages.
Project consists of two parts:
* `testhttp_raw` - client program that allows to test webpages using HTTP 1.1 protocol over TCP/IPv4
* `testhttp` - bash script that allows to test webpages using HTTPS by creating the tunnel between `testhttp_raw` and secured webpage

## testhttp_raw

Technical format of created requests and parsed responses was based on RFC7230.
Client program connects to specified address and port and then sends the HTTP request. Next task is to analyze the response from server and generate report.
The report is the listing of all cookies and the length of the response in bytes.

### Running testhttp_raw

The program can be run with
```testhttp_raw <connection address>:<port> <cookies file path> <tested http address>```
where
`connection address` - address of server application to connect to
`port` - port of server application to connect to
`cookies file path` - path to the file with cookies data to be sent to server with request
`tested http address` - address of the webpage to be tested (as there can be many pages hosted on single server)

for example:
```./testhttp_raw www.mimuw.edu.pl:80 cookies.txt http://www.mimuw.edu.pl/```
where `cookies.txt` can be an empty file but must exist in the filesystem.


## testhttp

This script gives the `testhttp_raw` ability to work using `HTTPS` protocol as well as `HTTP` protocol.
It recognizes the secured version of `HTTP` and when needed creates a tunnel using `stunnel` and redirects all communication via this tunnel.
It simplifies a little bit possibilities of `testhttp_raw` but gives it the ability to connect to many different pages.

### Running testhttp

The program can be run with
```testhttp <cookies file path> <tested http[s] address>```
where
`cookies file path` - path to the file with cookies data to be sent to server with request
`tested http[s] address` - address of the webpage to be tested (from which the address of the server will be obtained)

## Project compilation process

The project can be compiled using `Makefile` or `CMakeLists.txt`. To compile project with prefered option using `Makefile` just type `make` in project directory.
